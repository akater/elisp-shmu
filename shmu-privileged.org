# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: shmu-privileged
#+subtitle: Part of the =shmu= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle (if (memq 'privileged ob-flags) "shmu-privileged.el" "") :mkdirp yes :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

This component is shipped with the package but is not required on package load.

* Require
#+begin_src elisp :results none
(require 'tramp)
(require 'tramp-sh)
#+end_src

Whenever this package is used, user has to manually
#+begin_src elisp :tangle no :results none
(defvar sudo-edit-local-method)
#+end_src
(This does not help on load.)

We need ~sudo-edit~ solely to get access to user's preference for local do-it-as-someone-else program.  Sadly, no built-in variable is provided.

We do not ~(require 'sudo-edit)~ but it should be listed as a runtime dependency for ~shmu-privileged~.

With CL packages, we'd probably have to require ~sudo-edit~ here on load.

* with-sudo
** Definition
#+begin_src elisp :results none
(defmacro with-sudo (&rest body)
  (declare (indent 0))
  `(let ((default-directory "/sudo::/")) ,@body))
#+end_src

* with-sudo-i
** Prerequisites
*** =tramp= settings
#+begin_src elisp :results none
(add-to-list 'tramp-methods
	     '("sudo-i"
	       (tramp-login-program        "sudo")
	       ;; The password template must be masked.  Otherwise, it could be
	       ;; interpreted as password prompt if the remote host echoes the command.
	       (tramp-login-args           (("-i") ("-u" "%u") ("-H")
					    ("-p" "P\"\"a\"\"s\"\"s\"\"w\"\"o\"\"r\"\"d\"\":")))
	       ;; Local $SHELL could be a nasty one, like zsh or fish.  Let's override it.
	       (tramp-login-env            (("SHELL") ("/bin/sh")))
	       (tramp-remote-shell         "/bin/sh")
	       (tramp-remote-shell-login   ("-l"))
	       (tramp-remote-shell-args    ("-c"))
	       (tramp-connection-timeout   10)))

(tramp-set-completion-function "sudo-i" tramp-completion-function-alist-su)
#+end_src

** Definition
#+begin_src elisp :results none
(defmacro with-sudo-i (&rest body)
  (declare (indent 0))
  `(let ((default-directory "/sudo-i::/")) ,@body))
#+end_src

* with-privileged
** Prerequisites
#+begin_src elisp :results none
(defcustom shmu-privileged-tramp-method "sudo"
  "Tramp method to employ for running code with elevated permissions.

Namely, applies to the code in bodies of `shmu-with-privileged'."
  :type '(choice (const "sudo") (const "doas") (const "su"))
  :group 'tramp)
#+end_src

** Definition
#+begin_src elisp :results none
(defmacro shmu-with-privileged (&rest body)
  "Evaluate BODY forms with extended user capabilities.

Protocol is determined by `sudo-edit-local-method'."
  (declare (indent 0))
  `(let ((default-directory (concat "/" sudo-edit-local-method
                                    "::/")))
     ;; We should handle errors here but I don't know how to do it.
     ,@body))
#+end_src

* with-privileged-if
** Definition
#+begin_src elisp :results none
(defmacro shmu-with-privileged-if (test &rest body)
  "Evaluate BODY forms, with special user capabilities if TEST is non-nil.

Protocol is determined by `sudo-edit-local-method'."
  (declare (indent 1))
  `(if ,test (shmu-with-privileged ,@body)
     ,@body))
#+end_src
