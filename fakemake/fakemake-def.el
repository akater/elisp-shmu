;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'shmu
  authors "Dima Akater"
  first-publication-year-as-string "2019"
  org-files-in-order '("shmu-util"
                       "shmu-env"
                       "shmu-wrap"
                       "shmu-privileged"
                       "shmu")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda () (require 'org-src-elisp-extras)))
